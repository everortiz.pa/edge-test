
# Edge-test API - Recharge System

User recharge system to debit and credit.

## Install and run

- Install all project dependencies 
```sh
npm update
```

- Run project
```sh
npm run test
or 
node index.js
```


## Swagger docs

http://localhost:4000/api-docs/

## Main Functions

- Credit: The amount is credited by the **type** of credit. When making an accreditation, it is sought if there is a benefit for the type of credit and if it meets the conditions of **toUserType** and **minimumRecharge**, the **rewardAmount** is added to the total amount.
- Debit: The amount is debited by the type of credit