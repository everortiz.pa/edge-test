const API_VERSION = "v1";
const PORT = 4000;
const PROJECT_NAME = "edge-test";

module.exports = {
    API_VERSION,
    PORT,
    PROJECT_NAME
};