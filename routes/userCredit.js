const express = require('express');
const UserCreditController = require('../controllers/userCredit');

const api = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     UserCredit:
 *       type: object
 *       required:
 *         - user_id
 *         - amount
 *         - type
 *       properties:
 *         user_id:
 *           type: string
 *           description: The user id
 *         amount:
 *           type: number
 *           description: amount of credit available
 *         type:
 *           type: string
 *           description: The credit type [CALL, DATA, SMS]
 *       example:
 *         user_id: d5fE_asz
 *         amount: 10
 *         type: CALL
 */

/**
 * @swagger
 * tags:
 *   name: UserCredit
 *   description: The users managing API
 */

/**
* @swagger
* /user/credit/{userId}:
*   get:
*     summary: Returns the list of all types of user credits by user_id
*     tags: [UserCredit]
*     parameters:
*       - in: path
*         name: userId
*     responses:
*       200:
*         description: The list of the credits users
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/components/schemas/User'
*/
api.get("/:userId", UserCreditController.getUserCredit);

/**
 * @swagger
 * /user/credit/accredit/{userId}/:
 *  put:
 *    summary: Execute a credit operation according to type and userId
 *    tags: [UserCredit]
 *    parameters:
 *      - in: path
 *        name: userId
 *        schema:
 *          type: string
 *          required: true
 *          description: The user id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/UserCredit'
 *    responses:
 *      200:
 *        description: Successful credit load
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserCredit'
 *      404:
 *        description: The user was not found
 *      500:
 *        description: Server error
 */
api.put("/accredit/:userId/", UserCreditController.accredit);

/**
 * @swagger
 * /user/credit/debit/{userId}/:
 *  put:
 *    summary: Execute a debit operation according to type and userId
 *    tags: [UserCredit]
 *    parameters:
 *      - in: path
 *        name: userId
 *        schema:
 *          type: string
 *          required: true
 *          description: The user id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/UserCredit'
 *    responses:
 *      200:
 *        description: Successful credit load
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserCredit'
 *      404:
 *        description: The user was not found
 *      500:
 *        description: Server error
 */
api.put("/debit/:userId/", UserCreditController.debit);

module.exports = api;