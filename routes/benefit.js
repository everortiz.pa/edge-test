const express = require('express');
const BenefitController = require('../controllers/benefit');

const api = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     Benefit:
 *       type: object
 *       required:
 *         - name
 *         - creditType
 *         - minimumRecharge
 *         - toUserType
 *         - rewardAmount
 *       properties:
 *         name:
 *           type: string
 *           description: Name of the benefit
 *         creditType:
 *           type: string
 *           description: Types of credit to access the benefit [CALL, DATA, SMS]
 *         minimumRecharge:
 *           type: number
 *           description: Minimum amount to access the benefit
 *         toUserType:
 *           type: string
 *           description: Types of users can access the benefit [Full, Basic]
 *         rewardAmount:
 *           type: string
 *           description: Gift amount
 *       example:
 *         name: gift_data
 *         creditType: DATA
 *         minimumRecharge: 100
 *         toUserType: FULL
 *         rewardAmount: 100
 */

/**
 * @swagger
 * tags:
 *   name: Benefit
 *   description: Benefits that users can access
 */

/**
* @swagger
* /benefits:
*   get:
*     summary: Returns the list of all benefits
*     tags: [Benefit]
*     responses:
*       200:
*         description: The benefits of the users
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/components/schemas/Benefit'
*/
api.get("/", BenefitController.getBenefits);


/**
 * @swagger
 * /benefits:
 *   post:
 *     summary: Create a new benefits
 *     tags: [Benefit]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Benefit'
 *     responses:
 *       200:
 *         description: The benefit was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Benefit'
 *       500:
 *         description: Name or type of credit already exist
 */
api.post("/", BenefitController.save);


/**
 * @swagger
 * /benefits/{name}:
 *  put:
 *    summary: Update the benefit by the name
 *    tags: [Benefit]
 *    parameters:
 *      - in: path
 *        name: name
 *        schema:
 *          type: string
 *        required: true
 *        description: The benefit name
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Benefit'
 *    responses:
 *      200:
 *        description: The benefit was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Benefit'
 *      404:
 *        description: The benefit was not found
 *      500:
 *        description: Error, the type of credit cannot be modified
 */
api.put("/:name", BenefitController.update);

/**
 * @swagger
 * /benefits/{name}:
 *   delete:
 *     summary: Remove the benefit by name
 *     tags: [Benefit]
 *     parameters:
 *       - in: path
 *         name: name
 *         schema:
 *           type: string
 *         required: true
 *         description: The benefit name
 * 
 *     responses:
 *       200:
 *         description: The benefit was deleted
 *       404:
 *         description: The benefit was not found
 */
api.delete("/:name", BenefitController.destroy);

module.exports = api;