function getBenefits(req, res) {
    const benefits = req.app.db.get("benefits")

    res.send(benefits)
}

function getBenefitByCreditType(req) {
    return req.app.db.get("benefits").find({ creditType: req.body.type }).value()
}


function save(req, res) {
    try {

        const check = checkBenefit(req, res)
        if (check.check) {
            req.app.db.get("benefits").push(req.body).write()
            res.send(req.body)
        } else {
            return res.status(500).send(check.message)
        }
    } catch (err) {
        return res.status(500).send(err)
    }
}

function checkBenefit(req, res) {
    const benefit = {
        ...req.body
    }
    let benefitDb = req.app.db.get("benefits").find({ creditType: benefit.creditType }).value()
    if (benefitDb != undefined && benefitDb.creditType == benefit.creditType)
        return { check: false, message: `error, the type of credit already exists ${benefit.creditType}` }

    benefitDb = req.app.db.get("benefits").find({ name: benefit.name }).value()
    if (benefitDb != undefined && benefitDb.name == benefit.name)
        return { check: false, message: `error, the name of credit already exists ${benefit.name}` }
    return { check: true, message: `success` }
}

function update(req, res) {
    try {
        const benefitDb = req.app.db.get("benefits").find({ name: req.params.name })
        if (!benefitDb.value())
            return res.sendStatus(404)
        if (req.body.creditType != undefined && req.body.creditType != benefitDb.value().creditType) {
            return res.status(500).send("Error, the type of credit cannot be modified")
        }
        benefitDb.assign(req.body).write()
        res.send(benefitDb)
    } catch (err) {
        return res.status(500).send(err)
    }
}

function destroy(req, res) {
    try {
        const user = req.app.db.get("benefits").find({ name: req.params.name })
        if (!user.value())
            return res.sendStatus(404)
        req.app.db.get("benefits").remove({ name: req.params.name }).write()
        res.sendStatus(200)
    } catch (err) {
        return res.status(500).send(err)
    }
}

module.exports = {
    getBenefits,
    save,
    update,
    destroy,
    getBenefitByCreditType
}