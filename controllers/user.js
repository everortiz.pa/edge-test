const { nanoid } = require('nanoid');
const idlength = 8;

function getUsers(req, res) {
    const users = req.app.db.get("users")

    res.send(users)
}

function getUserById(req, res) {
    const user = searchById(req)
    if (!user)
        res.sendStatus(404)

    res.send(user)
}

function searchById(req) {
    return req.app.db.get("users").find({ id: req.params.id }).value()
}

async function save(req, res) {
    try {
        const user = {
            id: nanoid(idlength),
            ...req.body
        }

        req.app.db.get("users").push(user).write()

        let userCredits = [
            {
                "user_id": user.id,
                "type": "CALL",
                "amount": 0
            },
            {
                "user_id": user.id,
                "type": "SMS",
                "amount": 0
            },
            {
                "user_id": user.id,
                "type": "DATA",
                "amount": 0
            }
        ]

        for (const uC of userCredits) {
            req.app.db.get("userCredit").push(uC).write()
        }
        res.send({ user, userCredits })
    } catch (err) {
        return res.status(500).send(err)
    }
}

function update(req, res) {
    try {
        const user = req.app.db.get("users").find({ id: req.params.id })
        if (!user.value())
            return res.sendStatus(404)
        user.assign(req.body).write()
        res.send(user)
    } catch (err) {
        return res.status(500).send(err)
    }
}

function destroy(req, res) {
    try {
        const user = req.app.db.get("users").find({ id: req.params.id })
        if (!user.value())
            return res.sendStatus(404)
        req.app.db.get("users").remove({ id: req.params.id }).write()
        req.app.db.get("userCredit").remove({ user_id: req.params.id }).write()
        res.sendStatus(200)
    } catch (err) {
        return res.status(500).send(err)
    }
}

module.exports = {
    getUsers,
    getUserById,
    save,
    update,
    destroy,
    searchById
};