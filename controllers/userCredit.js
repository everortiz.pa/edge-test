const BenefitController = require('./benefit');
const UserController = require('./user');

function getUserCredit(req, res) {
    const userCredit = req.app.db.get("userCredit").chain().filter({ user_id: req.params.userId }).value()
    if (userCredit.length == 0)
        return res.sendStatus(404)

    res.send(userCredit)
}

function accredit(req, res) {
    process(req, res, false)
}

function debit(req, res) {
    process(req, res, true)
}

function process(req, res, debit) {
    try {
        const userCredit = req.app.db.get("userCredit").chain().filter({ user_id: req.params.userId, type: req.body.type }).first()
        if (!userCredit.value())
            return res.sendStatus(404)

        if (debit)
            req.body.amount = userCredit.value().amount - req.body.amount
        else {
            const benefit = BenefitController.getBenefitByCreditType(req);
            req.params.id = userCredit.value().user_id
            const user = UserController.searchById(req);
            if (benefit != undefined && req.body.amount >= benefit.minimumRecharge && user.type == benefit.toUserType)
                req.body.amount = userCredit.value().amount + req.body.amount + benefit.rewardAmount
            else
                req.body.amount = userCredit.value().amount + req.body.amount
        }

        userCredit.assign(req.body).write()
        res.send(userCredit)
    } catch (err) {
        return res.status(500).send(err)
    }
}

module.exports = {
    getUserCredit,
    accredit,
    debit
};