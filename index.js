const express = require("express");
const morgan = require("morgan");
const low = require("lowdb");
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const { API_VERSION, PORT } = require('./config');

/**
 * To store data in a file
 */
const FileSync = require("lowdb/adapters/FileSync")
/**
 * Where store the file
 */
const adapter = new FileSync("db.json")
const db = low(adapter)

db.defaults({ users: [], userCredit: [], benefits: [] }).write()

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Library API",
            version: "1.0.0",
            description: "Edge test API - Recharge/Debit System"
        },
        servers: [
            {
                url: `http://localhost:${PORT}/api/${API_VERSION}/`
            }
        ]
    },
    apis: ["./routes/*.js"]
}

const specs = swaggerJsDoc(options)


const app = express();

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs))

app.db = db
app.use(express.json())
app.use(morgan("dev"))


// Load routing
const userRoutes = require('./routes/user')
const benefitsRoutes = require('./routes/benefit')
const userCreditRoutes = require('./routes/userCredit')


// Router basic
app.use(`/api/${API_VERSION}/users`, userRoutes)
app.use(`/api/${API_VERSION}/benefits`, benefitsRoutes)
app.use(`/api/${API_VERSION}/user/credit`, userCreditRoutes)

app.listen(PORT, () => console.log(`Server runing on http://localhost:${PORT}/api-docs/`))
module.exports = app;